<?php

namespace Database\Factories;

use App\Models\Character;
use Illuminate\Database\Eloquent\Factories\Factory;

class CharacterFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Character::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->name,
            'status_id' => $this->faker->numberBetween($min = 1, $max = 3), // We're creating 3 status on StatusSeeder
            'species' => $this->faker->sentence($nbWords = $this->faker->numberBetween($min = 1, $max = 3), $variableNbWords = true),
            'type' => $this->faker->sentence($nbWords = $this->faker->numberBetween($min = 1, $max = 3), $variableNbWords = true),
            'gender_id' => $this->faker->numberBetween($min = 1, $max = 4), // We're creating 4 genders on GenderSeeder
            'origin' => $this->faker->sentence($nbWords = $this->faker->numberBetween($min = 1, $max = 3), $variableNbWords = true),
            'image' => 'https://picsum.photos/150/?random&t=' . $this->faker->randomFloat($nbMaxDecimals = NULL, $min = 0, $max = NULL),
            'location_id' => $this->faker->numberBetween($min = 1, $max = 30), // We're creating 30 locations on LocationSeeder
        ];
    }
}
