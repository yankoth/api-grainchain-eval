<?php

namespace Database\Factories;

use App\Models\Location;
use Illuminate\Database\Eloquent\Factories\Factory;

class LocationFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Location::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->unique()->state,
            'type' => $this->faker->sentence($nbWords = $this->faker->numberBetween($min = 1, $max = 3), $variableNbWords = true),
            'dimension' => $this->faker->sentence($nbWords = $this->faker->numberBetween($min = 1, $max = 3), $variableNbWords = true),
            'url' => $this->faker->url,
        ];
    }
}
