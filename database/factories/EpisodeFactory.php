<?php

namespace Database\Factories;

use App\Models\Episode;
use Illuminate\Database\Eloquent\Factories\Factory;

class EpisodeFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Episode::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'season' => $this->faker->numberBetween($min = 1, $max = 15),
            'number' => $this->faker->numberBetween($min = 1, $max = 30),
            'name' => $this->faker->company,
            'air_date' => $this->faker->date($format = 'Y-m-d', $max = 'now'),
        ];
    }

    /**
     * Configure the model factory.
     *
     * @return $this
     */
    public function configure()
    {
        return $this->afterCreating(function (Episode $episode) {
            for ($i = 0; $i < $this->faker->numberBetween($min = 7, $max = 10); $i++) {
                $episode->characters()->attach($this->faker->numberBetween($min = 1, $max = 250)); // We're creating 250 characters on CharacterSeeder
            }
        });
    }
}
