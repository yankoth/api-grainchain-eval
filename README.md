# Prueba técnica web developer / GrainChain

// Instalar dependencias  
composer install  

// Crear y configurar ambiente  
cp .env.example .env  

// Generar llave  
php artisan key:generate  

// Correr migraciones  
php artisan migrate  

// Popular base de datos  
php artisan db:seed  

Nota: El CORS está configurado de manera pública intencionalmente, en producción se declararía los origins permitidos.  
Nota: Generalmente el trabajo va sobre tasks o historias de usuario y se realiza un commit/push por cada uno de estos. En este caso, por cuestiones de tiempo se hizo todo en un commit.  