<?php

namespace App\Http\Controllers;

use App\Models\Location;
use App\Models\Character;
use Illuminate\Http\Request;
use App\Http\Requests\CharacterSaveRequest;

class CharacterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // In case of filter, get the parameter with $request->input(<paramter>)
        // Then filter using the parameter with Character::where(<filter>)->get();

        $itemsPerPage = $request->input('items_per_page');

        return $itemsPerPage
            ? Character::paginate($itemsPerPage)
            : Character::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CharacterSaveRequest $request)
    {
        $validated = $request->validated();

        $character = Character::create($validated);

        return $character;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Character  $character
     * @return \Illuminate\Http\Response
     */
    public function show(Character $character)
    {
        return $character;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Character  $character
     * @return \Illuminate\Http\Response
     */
    public function update(CharacterSaveRequest $request, Character $character)
    {
        $validated = $request->validated();

        // Check if location exists or just add a new one
        $location = Location::where('name', $validated['location']['name'])->first();

        if (!$location)
        {
            $location = Location::create([
                'name' => $validated['location']['name']
            ]);
        }

        $validated['location_id'] = $location->id;

        $character->fill($validated);

        $character->save();

        return $character->load('location');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Character  $character
     * @return \Illuminate\Http\Response
     */
    public function destroy(Character $character)
    {
        $character->delete();

        return $character;
    }
}
