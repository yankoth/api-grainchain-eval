<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Requests\LoginRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function login(LoginRequest $request)
    {
    	$validated = $request->validated();

    	$user = User::where('email', $validated['email'])->first();

    	if (!$user || !Hash::check($validated['password'], $user->password))
    	{
	        return response()->json('Credenciales inválidas.', 401);
	    }

        $user->tokens()->delete();

        $user->sessionToken = $user->createToken('session-token')->plainTextToken;

        return $user;
    }

    public function logout()
    {
    	$user = Auth::user();

    	$user->tokens()->delete();

    	return response()->json('Sesión terminada.', 200);
    }
}
