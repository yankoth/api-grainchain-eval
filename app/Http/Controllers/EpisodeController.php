<?php

namespace App\Http\Controllers;

use App\Models\Episode;
use Illuminate\Http\Request;
use App\Http\Requests\EpisodeSaveRequest;

class EpisodeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // In case of filter, get the parameter with $request->input(<paramter>)
        // Then filter using the parameter with Episode::where(<filter>)->get();

        $itemsPerPage = $request->input('items_per_page');

        return $itemsPerPage
            ? Episode::paginate($itemsPerPage)
            : Episode::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EpisodeSaveRequest $request)
    {
        $validated = $request->validated();

        $episode = Episode::create($validated);

        $episode->characters()->sync($validated['characters_id']);

        return $episode;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Episode  $episode
     * @return \Illuminate\Http\Response
     */
    public function show(Episode $episode)
    {
        return $episode;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Episode  $episode
     * @return \Illuminate\Http\Response
     */
    public function update(EpisodeSaveRequest $request, Episode $episode)
    {
        $validated = $request->validated();

        $episode->fill($validated);

        $episode->save();

        $episode->characters()->sync($validated['characters_id']);

        return $episode;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Episode  $episode
     * @return \Illuminate\Http\Response
     */
    public function destroy(Episode $episode)
    {
        $episode->delete();

        return $episode;
    }
}
