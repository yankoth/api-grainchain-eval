<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CharacterSaveRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'string|required|max:150',
            'status_id' => 'integer|required|exists:App\Models\Status,id',
            'species' => 'string|nullable|max:150',
            'type' => 'string|nullable|max:150',
            'gender_id' => 'integer|nullable|exists:App\Models\Gender,id',
            'origin' => 'string|nullable|max:150',
            'image' => 'url|required',
            'location_id' => 'integer|required|exists:App\Models\Location,id',
            'location.name' => 'string|required|max:150',
        ];
    }
}
