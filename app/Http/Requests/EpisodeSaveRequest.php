<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EpisodeSaveRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'season' => 'integer|required|min:0|max:1000',
            'number' => 'integer|required|min:0|max:1000',
            'name' => 'string|required|max:150',
            'air_date' => 'date|required',
            'characters_id' => 'array|required',
            'characters_id.*' => 'integer|required|exists:App\Models\Character,id',
        ];
    }
}
