<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Character extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'status_id',
        'species',
        'type',
        'gender_id',
        'origin',
        'image',
        'location_id',
    ];

    protected $with = ['status', 'gender', 'location'];

    public function status()
    {
        return $this->belongsTo(Status::class);
    }

    public function gender()
    {
        return $this->belongsTo(Gender::class);
    }

    public function episodes()
    {
        return $this->belongsToMany(Episode::class);
    }

    public function location()
    {
        return $this->belongsTo(Location::class);
    }
}
