<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Episode extends Model
{
    use HasFactory;

    protected $fillable = [
    	'season',
    	'number',
        'name',
        'air_date',
    ];

    protected $with = ['characters'];

    public function characters()
    {
        return $this->belongsToMany(Character::class);
    }
}
