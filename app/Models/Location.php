<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'type',
        'dimension',
        'url'
    ];

    // protected $with = ['residents'];

    public function residents()
    {
        return $this->hasMany(Character::class);
    }
}
