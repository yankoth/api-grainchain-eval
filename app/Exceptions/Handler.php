<?php

namespace App\Exceptions;

use Throwable;
use Illuminate\Database\QueryException;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Validation\ValidationException;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->renderable(function (ValidationException $exception, $request) {
            $errors = $exception->validator->errors()->getMessages();

            $errorsString = '';

            foreach ($errors as $key => $value) {
                $errorsString .= $errors[$key][0] . '<br>';
            }

            $errorsString = substr($errorsString, 0, -4);
            
            return response()->json($errorsString, 422);
        });

        $this->renderable(function (ModelNotFoundException $exception, $request) {
            $model = strtolower(class_basename($exception->getModel()));

            return response()->json('Model not found.', 404);
        });

        $this->renderable(function (AuthenticationException $exception, $request) {
            return response()->json('Unauthenticated.', 401);
        });

        $this->renderable(function (AuthorizationException $exception, $request) {
            return response()->json('Unauthorized.', 403);
        });

        $this->renderable(function (NotFoundHttpException $exception, $request) {
            return response()->json('Url not found.', 404);
        });

        $this->renderable(function (MethodNotAllowedHttpException $exception, $request) {
            return response()->json('Method not allowed.', 405);
        });

        $this->renderable(function (HttpException $exception, $request) {
            return response()->json($exception->getMessage(), $exception->getStatusCode());
        });
    }
}
