<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\EpisodeController;
use App\Http\Controllers\LocationController;
use App\Http\Controllers\CharacterController;

Route::middleware(['guest'])->group(function ()
{
	Route::post('login', [AuthController::class, 'login'])->name('login');
});

Route::middleware(['auth:sanctum'])->group(function ()
{
	Route::post('logout', [AuthController::class, 'logout'])->name('logout');

	Route::apiResource('statuses', LocationController::class)->only(['index']);
	Route::apiResource('genders', LocationController::class)->only(['index']);
	Route::apiResource('locations', LocationController::class);
	Route::apiResource('characters', CharacterController::class);
	Route::apiResource('episodes', EpisodeController::class);
});
